<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VocabWord extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'word_string'
    ];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the video record associated with the VocabWord.
     */
    public function video()
    {
        return $this->hasOne(Video::class);
    }

    public function lessonLevel()
    {
        return $this->belongsTo(LessonLevel::class);
    }

}
