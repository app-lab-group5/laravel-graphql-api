<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_path'
    ];

    /**
     * Get the VocabWord that belongs to the Video.
     */
    public function vocabWord()
    {
        return $this->belongsTo(VocabWord::class);
    }

    public function getVideoPathAttribute($value)
    {
        return env('ASSETS_URL') . $value;
    }
}
