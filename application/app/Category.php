<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the vocab_words for the category.
     */
    public function vocabWords()
    {
        return $this->hasMany(VocabWord::class);
    }

    /**
     * Get the lesson for the category.
     */
    public function lesson()
    {
        return $this->hasOne(Lesson::class);
    }
}
