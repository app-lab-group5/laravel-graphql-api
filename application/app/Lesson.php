<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the levels for the lesson.
     */
    public function lessonLevels()
    {
        return $this->hasMany(LessonLevel::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
