<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonLevel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'difficulty_level'
    ];

    /**
     * Get the levels for the lesson.
     */
    public function vocabWords()
    {
        return $this->hasMany(VocabWord::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }
}
