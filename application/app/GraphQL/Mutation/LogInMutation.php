<?php

namespace App\GraphQL\Mutation;

use App\ConstraintFlip;
use App\ConstraintToken;
use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class LogInMutation extends Mutation
{
    protected $attributes = [
        'name' => 'logIn'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'email' => [
                'name' => 'email',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'email'],
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $credentials = [
            'email' => $args['email'],
            'password' => $args['password']
        ];

        $token = auth()->attempt($credentials);

        if (!$token) {
            throw new \Exception('Unauthorized!');
        }

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser($token);

        if ($user->constraint === null){
            $constraintFlip = ConstraintFlip::find(1);
            $user->constraint = $constraintFlip->getFlipAsInteger();
            $constraintFlip->flip();
            $constraintFlip->save();
            $user->save();
        }

        $constraintToken = new ConstraintToken();
        $constraintToken->fill([
            'token' => $token,
            'constraint' => $user->constraint
        ]);

        return $token . " " . $user->constraint;
    }
}