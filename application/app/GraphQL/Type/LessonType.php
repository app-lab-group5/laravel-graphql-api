<?php

namespace App\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class LessonType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Lesson',
        'description' => 'A lesson'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the category'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the lesson'
            ],
            'category' => [
                'type' => GraphQL::type('Category'),
                'description' => 'The category of a lesson'
            ],
            'lessonLevels' => [
                'type' => Type::listOf(GraphQL::type('LessonLevel')),
                'description' => 'The different levels for a lesson'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a lesson was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a lesson was updated'
            ],
        ];
    }

// If you want to resolve the field yourself, you can declare a method
// with the following format resolve[FIELD_NAME]Field()
    protected function resolveNameField($root, $args)
    {
        return strtolower($root->name);
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at;
    }

}

