<?php

namespace App\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class LessonLevelType extends GraphQLType
{
    protected $attributes = [
        'name' => 'LessonLevel',
        'description' => 'A level of a given lesson'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the category'
            ],
            'difficulty_level' => [
                'type' => Type::int(),
                'description' => 'The difficulty of a given lesson level'
            ],
            'lesson' => [
                'type' => GraphQL::type('Lesson'),
                'description' => 'The lesson for a given lesson Level'
            ],
            'vocabWords' => [
                'type' => Type::listOf(GraphQL::type('VocabWord')),
                'description' => 'The vocabWords for a lessonLevel'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a lessonLevel was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a lessonLevel was updated'
            ],
        ];
    }

// If you want to resolve the field yourself, you can declare a method
// with the following format resolve[FIELD_NAME]Field()
    protected function resolveDifficultyLevelField($root, $args)
    {
        return strtolower($root->difficulty_level);
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at;
    }

}

