<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ConstraintTokenType extends GraphQLType
{
    protected $attributes = [
        'name' => 'ConstraintToken',
        'description' => 'Container class for token and constraint'
    ];

    public function fields()
    {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Authentication token for a given user'
            ],
            'constraint' => [
                'type' => Type::int(),
                'description' => 'Constraint which is indicating which group a user belongs to'
            ],
        ];
    }

    protected function resolveTokenField($root, $args)
    {
        return (string)$root->token;
    }

    protected function resolveConstraintField($root, $args)
    {
        return (int)$root->constraint;
    }

}