<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'A user'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user'
            ],
            'first_name' => [
                'type' => Type::string(),
                'description' => 'The first name of the user'
            ],
            'last_name' => [
                'type' => Type::string(),
                'description' => 'The last name of the user'
            ],
            'constraint' => [
                'type' => Type::int(),
                'description' => 'Constraint which is indicating the group a user belongs to'
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The email of the user'
            ],
            'bits' => [
                'type' => Type::listOf(GraphQL::type('Bit')),
                'description' => 'The user bits'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a user was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a user was updated'
            ],
        ];
    }

// If you want to resolve the field yourself, you can declare a method
// with the following format resolve[FIELD_NAME]Field()
    protected function resolveEmailField($root, $args)
    {
        return strtolower($root->email);
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at;
    }

}

