<?php

namespace App\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class VocabWordType extends GraphQLType
{
    protected $attributes = [
        'name' => 'VocabWord',
        'description' => 'VocabWord'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of a vocabWord'
            ],
            'word_string' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The corresponding word as string'
            ],
            'category' => [
                'type' => GraphQL::type('Category'),
                'description' => 'The category for a vocabWord'
            ],
            'video' => [
                'type' => GraphQL::type('Video'),
                'description' => 'The video for a vocabWord'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a bit was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a bit was updated'
            ],
        ];
    }

    protected function resolveWordStringField($root, $args)
    {
        return (string)$root->word_string;
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string)$root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string)$root->updated_at;
    }
}