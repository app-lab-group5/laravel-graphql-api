<?php

namespace App\GraphQL\Query;

use App\VocabWord;
use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class VocabWordsQuery extends Query
{
    protected $attributes = [
        'name' => 'vocabWords'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('VocabWord'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'word_string' => ['name' => 'word_string', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if (isset($args['id'])) {
            return VocabWord::where('id', $args['id'])->get();
        } else if (isset($args['word_string'])) {
            return VocabWord::where('word_string', $args['word_string'])->get();
        } else {
            return VocabWord::all();
        }
    }
}



