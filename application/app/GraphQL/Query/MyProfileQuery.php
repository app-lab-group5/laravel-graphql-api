<?php

namespace App\GraphQL\Query;

use GraphQL;
use App\Bit;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class MyProfileQuery extends Query
{
    protected $attributes = [
        'name' => 'myProfile'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'first_name' => ['name' => 'first_name', 'type' => Type::string()],
            'last_name' => ['name' => 'last_name', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()]
        ];
    }


    public function authenticated($root, $args, $currentUser)
    {
        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        return \Auth::user();
    }
}