<?php

namespace App\GraphQL\Query;

use App\Video;
use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class VideosQuery extends Query
{
    protected $attributes = [
        'name' => 'videos'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Video'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'video_path' => ['name' => 'video_path', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if (isset($args['id'])) {
            return Video::where('id', $args['id'])->get();
        } else if (isset($args['video_path'])) {
            return Video::where('video_path', $args['video_path'])->get();
        } else {
            return Video::all();
        }
    }
}



