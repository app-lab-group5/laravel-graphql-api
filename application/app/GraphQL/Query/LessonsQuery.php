<?php

namespace App\GraphQL\Query;

use App\Lesson;
use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class LessonsQuery extends Query
{
    protected $attributes = [
        'name' => 'lessons'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Lesson'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'name' => ['name' => 'name', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return Lesson::where('id', $args['id'])->get();
        } else if (isset($args['name'])) {
            return Lesson::where('name', $args['name'])->get();
        } else {
            return Lesson::all();
        }
    }
}



