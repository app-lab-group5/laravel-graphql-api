<?php

namespace App\GraphQL\Query;

use App\LessonLevel;
use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class LessonLevelsQuery extends Query
{
    protected $attributes = [
        'name' => 'lessonLevels'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('LessonLevel'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'difficulty_level' => ['name' => 'difficulty_level', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return LessonLevel::where('id', $args['id'])->get();
        } else if (isset($args['name'])) {
            return LessonLevel::where('difficulty_level', $args['difficulty_level'])->get();
        } else {
            return LessonLevel::all();
        }
    }
}



