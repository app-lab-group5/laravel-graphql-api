<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConstraintFlip extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flip',
    ];

    public function flip() {
        $this->flip = !$this->flip;
    }

    public function getFlipAsInteger() {
        if ($this->flip) return 1;
        return 0;
    }

}
