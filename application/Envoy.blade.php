/*
|--------------------------------------------------------------------------
| Envoy Bootstrapping
|--------------------------------------------------------------------------
|
| If a kind of setup is necessary before executing the tasks, here's the
| right place for that.
|
| SSH-Keys: You have to setup the SSH Keys on your machine and the server
| to make envoy work
|
*/

/*
|--------------------------------------------------------------------------
| TODO
|--------------------------------------------------------------------------
|
| * Create new user deployer on remote machine and use SSH keys for deployment
| * Fix the deploy and initial-deploy tasks and make them work with docker
|
|
*/


@setup
$now = new DateTime();
//$environment = isset($env) ? $env : "testing";
@endsetup


/*
|--------------------------------------------------------------------------
| Envoy Tasks
|--------------------------------------------------------------------------
|
| Envoy supports a nice way to manage deployment tasks by using a SSH
| connection to your server and executing tasks like git pull origin
|
*/

@servers(['cschmidl' => 'cschmidl@cschmidl.com'])

@task('deploy', ['on' => 'cschmidl', 'confirm' => true])
cd /var/www/laravel-graphql-api
git checkout
git pull origin master
composer install --no-dev --optimize-autoloader
chgrp www-data ./ -R
chmod -R 775 storage/
chmod 775 storage/logs/laravel.log
php artisan cache:clear
php artisan config:clear
composer dump-autoload
@endtask


@task('initial-deploy', ['on' => 'cschmidl', 'confirm' => true])
cd /var/www/laravel-graphql-api
git pull origin master
git submodule update --init
cd docker-environment
cp env-example .env
docker-compose up -d nginx mysql workspace

/* composer install --no-dev --optimize-autoloader
cd ..
npm cache clean
cp .env.example .env
npm install --production
chgrp www-data ./ -R
chmod -R 775 storage/
chmod 775 storage/logs/laravel.log
php artisan key:generate
php artisan jwt:secret
php artisan cache:clear
php artisan queue:restart */
@endtask

@task('start_docker', ['on' => 'cschmidl', 'confirm' => true])
cd /var/www/laravel-graphql-api/docker-environment/
docker-compose up -d nginx mysql workspace
@endtask

@task('stop_docker', ['on' => 'cschmidl', 'confirm' => true])
cd /var/www/laravel-graphql-api/docker-environment/
docker-compose down
@endtask

// docker exec -it --user=laradock dockerenvironment_workspace_1 bash