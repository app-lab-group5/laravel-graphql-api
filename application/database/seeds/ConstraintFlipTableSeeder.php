<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConstraintFlipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('constraint_flips')->insert([
            'flip' => true,
        ]);
    }
}
