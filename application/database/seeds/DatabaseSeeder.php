<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(VocabWordsTableSeeder::class);
        $this->call(VideosTableSeeder::class);
        $this->call(LessonsTableSeeder::class);
        $this->call(LessonLevelsTableSeeder::class);
        $this->call(ConstraintFlipTableSeeder::class);
    }
}
