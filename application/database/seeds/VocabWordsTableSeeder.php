<?php

use App\Category;
use App\VocabWord;
use Illuminate\Database\Seeder;

class VocabWordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            "buildings" => [
                "office" => "office.mp4",
                "hotel" => "hotel.mp4",
                "hospital" => "hospital.mp4",
                "church" => "church.mp4",
                "bank" => "bank.mp4",
                "movie theater" => "movie_theater.mp4",
                "museum" => "museum.mp4",
                "restaurant" => "restaurant.mp4",
                "school" => "school.mp4",
                "station" => "station.mp4",
                "theater" => "theater.mp4",
                "university" => "university.mp4"
            ],
            "emotions" => [
                "shy" => "shy.mp4",
                "sad" => "sad.mp4",
                "nervous" => "nervous.mp4",
                "in love" => "in_love.mp4",
                "jealous" => "jealous.mp4",
                "angry" => "angry.mp4",
                "bad" => "bad.mp4",
                "boring" => "boring.mp4",
                "calm" => "calm.mp4",
                "curious" => "curious.mp4",
                "funny" => "funny.mp4",
                "happy" => "happy.mp4",
                "content" => "happy(gelukkig).mp4",
                "pain" => "pain.mp4",
                "proud" => "proud.mp4",

            ],
            "family-friends" => [
                "woman" => "woman.mp4",
                "wife" => "wife.mp4",
                "son" => "son.mp4",
                "sister" => "sister.mp4",
                "partner" => "partner.mp4",
                "male friend" => "male_friend.mp4",
                "girlfriend" => "girlfriend.mp4",
                "friend" => "friend.mp4",
                "female friend" => "female_friend.mp4",
                "father" => "father.mp4",
                "child" => "child.mp4",
                "aunt" => "aunt.mp4",
                "brother" => "brother.mp4",
                "daughter" => "daughter.mp4",
                "family" => "family.mp4",
                "grandma" => "grandma.mp4",
                "grandpa" => "grandpa.mp4",
                "husband" => "husband.mp4",
                "man" => "man.mp4",
                "mother" => "mother.mp4",
                "parents" => "parents.mp4",
                "people" => "people.mp4",
                "person" => "person.mp4",
                "uncle" => "uncle.mp4",
            ],
            "food" => [
                "sausage" => "sausage.mp4",
                "meat" => "meat.mp4",
                "jam" => "jam.mp4",
                "cookie" => "cookie.mp4",
                "cheese" => "cheese.mp4",
                "bread" => "bread.mp4",
                "butter" => "butter.mp4",
                "chocolat" => "chocolat.mp4",
                "donut" => "donut.mp4",
                "egg" => "egg.mp4",
                "orange (fruit)" => "orange(fruit).mp4",
                "pie" => "pie.mp4",
                "sugar" => "sugar.mp4",
            ],
            "holidays" => [
                "vacation" => "vacation.mp4",
                "christmas" => "christmas.mp4",
                "easter" => "easter.mp4",
                "newyear" => "newyear.mp4",
            ],
            "home" => [
                "toilet" => "toilet.mp4",
                "room" => "room.mp4",
                "living" => "living.mp4",
                "kitchen" => "kitchen.mp4",
                "address" => "address.mp4",
                "bed" => "bed.mp4",
                "chair" => "chair.mp4",
                "door" => "door.mp4",
                "home" => "home.mp4",
                "roof" => "roof.mp4",
                "table" => "table.mp4",
                "television" => "television.mp4",
                "window" => "window.mp4",
            ],
            "jobs" => [
                "director" => "ceo_director.mp4",
                "baker" => "baker.mp4",
                "butcher" => "butcher.mp4",
                "dentist" => "dentist.mp4",
                "doctor" => "docter.mp4",
                "job" => "job.mp4",
                "police" => "police.mp4",
                "teacher" => "teacher.mp4",
            ],
            "other" => [
                "world" => "world.mp4",
                "weather" => "weather.mp4",
                "vegetarian" => "vegetarian.mp4",
                "too early" => "too_early.mp4",
                "question(s)" => "question(s).mp4",
                "media" => "media.mp4",
                "interesting" => "interesting.mp4",
                "hot" => "hot.mp4",
                "from you/yours" => "from_you_yours.mp4",
                "from me/mine" => "from_me_mine.mp4",
                "free" => "free.mp4",
                "early" => "early.mp4",
                "cold" => "cold.mp4",
                "birthday" => "birthday.mp4",
                "a lot" => "a_lot.mp4",
                "already" => "already.mp4",
                "also" => "also.mp4",
                "always" => "always.mp4",
                "another" => "another.mp4",
                "answer" => "answer.mp4",
                "beautiful" => "beautiful.mp4",
                "better" => "better.mp4",
                "break" => "break.mp4",
                "construction" => "construction.mp4",
                "culture" => "culture.mp4",
                "dead" => "dead.mp4",
                "deaf" => "deaf.mp4",
                "fight(n)" => "fight(n).mp4",
                "flower" => "flower.mp4",
                "god" => "god.mp4",
                "good/correct" => "good_correct.mp4",
                "good luck" => "good_luck.mp4",
                "hard of hearing" => "hard_of_hearing.mp4",
                "if" => "if.mp4",
                "ill(ness)" => "ill(ness).mp4",
                "important" => "important.mp4",
                "joke" => "joke.mp4",
                "letter" => "letter.mp4",
                "life" => "life.mp4",
                "mail" => "mail.mp4",
                "maybe" => "maybe.mp4",
                "money" => "money.mp4",
                "name" => "name.mp4",
                "new" => "new.mp4",
                "number" => "number.mp4",
                "old" => "old.mp4",
                "or" => "or.mp4",
                "please" => "please.mp4",
                "politics" => "politics.mp4",
                "printer" => "printer.mp4",
                "sex" => "sex.mp4",
                "sign language" => "sign_language.mp4",
            ],
            "time" => [
                "afternoon" => "afternoon.mp4",
                "april" => "april.mp4",
                "august" => "august.mp4",
                "date" => "date.mp4",
                "day" => "day.mp4",
                "december" => "december.mp4",
                "evening" => "evening.mp4",
                "february" => "februari.mp4",
                "friday" => "friday.mp4",
                "good afternoon" => "good_afternoon.mp4",
                "good evening" => "good_evening.mp4",
                "good morning" => "good_morning.mp4",
                "good night" => "good_night.mp4",
                "hour" => "hour.mp4",
                "january" => "januari.mp4",
                "july" => "july.mp4",
                "june" => "june.mp4",
                "march" => "march.mp4",
                "may" => "may.mp4",
                "monday" => "monday.mp4",
                "month" => "month.mp4",
                "morning" => "morning.mp4",
                "november" => "november.mp4",
                "now" => "now.mp4",
                "october" => "october.mp4",
                "saturday" => "saturday.mp4",
                "september" => "september.mp4",
                "spring" => "spring.mp4",
                "summer" => "summer.mp4",
                "sunday" => "sunday.mp4",
                "thursday" => "thursday.mp4",
                "today" => "today.mp4",
                "tuesday" => "tuesday.mp4",
                "wednesday" => "wednesday.mp4",
                "week" => "week.mp4",
                "weekend" => "weekend.mp4",
                "winter" => "winter.mp4",
                "year" => "year.mp4",
            ],
            "vehicles" => [
                "airplane" => "airplane.mp4",
                "bicycle" => "bicycle.mp4",
                "bus" => "bus.mp4",
                "car" => "car.mp4",
                "metro" => "metro.mp4",
                "train" => "train.mp4",
            ],
            "verbs" => [
                "(to) loan" => "loan(vn).mp4",
                "(to) mistake" => "(to)_mistake.mp4",
                "(to) soccer" => "(to)_soccer.mp4",
                "(to) work" => "(to)_work.mp4",
                "to boil" => "boil.mp4",
                "to build" => "build.mp4",
                "can" => "can(v).mp4",
                "cannot" => "cannot.mp4",
                "to cook" => "cook(v).mp4",
                "to dream" => "dream.mp4",
                "to drink" => "drink.mp4",
                "to drive" => "drive.mp4",
                "to eat" => "eat.mp4",
                "to email" => "email.mp4",
                "to fall" => "fall.mp4",
                "(to) hear(ing)" => "hear(ing).mp4",
                "to help" => "help(v).mp4",
                "to know" => "know.mp4",
                "to learn" => "learn.mp4",
                "to marry" => "marry.mp4",
                "to mean" => "mean(v).mp4",
                "to not care" => "not_care.mp4",
                "to read" => "read.mp4",
                "to sleep" => "sleep.mp4",
                "to sport" => "sport.mp4",
                "to start" => "start(nv).mp4",
                "to search" => "to_search.mp4",
                "to sit" => "to_sit.mp4",
                "to walk" => "to_walk.mp4",
                "to travel" => "travel.mp4",
                "to understand" => "understand.mp4",
                "to write" => "write.mp4",
            ],
            "animals" => [
                "bird" => "bird.mp4",
                "dog" => "dog.mp4",
                "hamster" => "hamster.mp4",
                "cat" => "kat.mp4",
                "mouse" => "mouse.mp4",
                "parrot" => "parrot.mp4",
                "rabbit" => "rabbit.mp4",
                "rat" => "rat.mp4",
            ],
            "colours" => [
                "black" => "black.mp4",
                "brown" => "brown.mp4",
                "green" => "green.mp4",
                "grey" => "grey.mp4",
                "orange (colour)" => "orange(color).mp4",
                "pink" => "pink.mp4",
                "purple" => "purple.mp4",
                "red" => "red.mp4",
                "white" => "white.mp4",
            ],
            "drinks" => [
                "beer" => "beer(tap).mp4",
                "coffee" => "coffee.mp4",
                "juice" => "juice.mp4",
                "milk" => "milk.mp4",
                "tea" => "tea.mp4",
                "water" => "water.mp4",
                "wine" => "wine.mp4",
            ],
            "pronounce" => [
                "how much/many" => "how_much_many.mp4",
                "how" => "how.mp4",
                "what" => "what.mp4",
                "when" => "when.mp4",
                "where" => "where.mp4",
                "which" => "which.mp4",
                "who" => "who.mp4",
                "why" => "why.mp4",
            ],
        ];


        /*


         "emotions" => [
            ],
            "familiy-friends" => [

            ],
            "food" => [

            ],
            "holidays" => [

            ],
            "home" => [

            ],
            "jobs" => [

            ],
            "other" => [

            ],
            "time" => [

            ],
            "vehicles" => [

            ],
            "verbs" => [

            ],
            "animals" => [

            ],
            "colours" => [

            ],
            "drinks" => [

            ],
            "pronounce" => [

            ],



         */





        /*
        {"word":"Appel","category":"Food","video_name":"appel", "level":1},
        {"word":"Boter","category":"Food","video_name":"boter", "level":1},
        {"word":"Brood","category":"Food","video_name":"brood", "level":1},
        {"word":"Jam","category":"Food","video_name":"jam", "level":1},
        {"word":"Melk","category":"Food","video_name":"melk", "level":1},
        {"word":"Kaas","category":"Food","video_name":"kaas", "level":1},

        {"word":"Alcohol","category":"Food","video_name":"alcohol", "level":2},
        {"word":"Cola","category":"Food","video_name":"cola", "level":2},
        {"word":"Eten","category":"Food","video_name":"eten", "level":2},
        {"word":"Fruit","category":"Food","video_name":"fruit", "level":2},
        {"word":"Honger","category":"Food","video_name":"honger", "level":2},

        {"word":"Keuken","category":"Food","video_name":"keuken", "level":3},
        {"word":"Kiwi","category":"Food","video_name":"kiwi", "level":3},
        {"word":"Koek","category":"Food","video_name":"koek", "level":3},
        {"word":"Koffie","category":"Food","video_name":"koffie", "level":3},
        {"word":"Lekker","category":"Food","video_name":"lekker", "level":3},


        {"word":"Blauw","category":"Colors","video_name":"blauw", "level":1},
        {"word":"Geel","category":"Colors","video_name":"geel", "level":1},
        {"word":"Groen","category":"Colors","video_name":"groen", "level":1},
        {"word":"Rood","category":"Colors","video_name":"rood", "level":1},
        {"word":"Oranje","category":"Colors","video_name":"oranje", "level":1},

        {"word":"Zwart","category":"Colors","video_name":"zwart", "level":2},
        {"word":"Bruin","category":"Colors","video_name":"bruin", "level":2},
        {"word":"Wit","category":"Colors","video_name":"wit", "level":2},
        {"word":"Roze","category":"Colors","video_name":"roze", "level":2},
        {"word":"Paars","category":"Colors","video_name":"paars", "level":2},


        {"word":"Hond","category":"Animals","video_name":"hond", "level":1},
        {"word":"Kat","category":"Animals","video_name":"kat", "level":1},
        {"word":"Vis","category":"Animals","video_name":"vis", "level":1},
        {"word":"Vogel","category":"Animals","video_name":"vogel", "level":1},
        {"word":"Koe","category":"Animals","video_name":"koe", "level":1},
        */

        $this->seedVocabWords($categories);
    }


    private function seedVocabWords($categoryArray) {

        foreach($categoryArray as $categoryName => $wordArray) {

            $category = Category::where('name', $categoryName)->first();

            foreach($wordArray as $word => $video) {
                $category->vocabWords()->create([
                    'word_string' => $word
                ]);
            }
        }
    }
}
