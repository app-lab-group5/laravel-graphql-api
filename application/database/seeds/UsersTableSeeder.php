<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Klaus',
            'last_name' => 'Lux',
            'email' => 'k.lux@signlanguageapp.com',
            'password' => bcrypt('secretklaus'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'first_name' => 'Justine',
            'last_name' => 'Winkler',
            'email' => 'j.winkler@signlanguageapp.com',
            'password' => bcrypt('secretjustine'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'first_name' => 'Christoph',
            'last_name' => 'Schmidl',
            'email' => 'c.schmidl@signlanguageapp.com',
            'password' => bcrypt('secretchristoph'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'first_name' => 'Lisette',
            'last_name' => 'Boeijenk',
            'email' => 'l.boeijenk@signlanguageapp.com',
            'password' => bcrypt('secretlisette'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'first_name' => 'Nicole',
            'last_name' => 'van de Raa',
            'email' => 'n.vanderaa@signlanguageapp.com',
            'password' => bcrypt('secretnicole'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


        // Participants

        // Joost Reumkens?
        DB::table('users')->insert([
            'first_name' => 'joost1',
            'last_name' => 'app',
            'email' => 'crapmail91@gmail.com',
            'password' => '$2y$10$e2.ZKF7bWFxOoC.sIOAlhuOBuDDFqiZmqMswT8/mPY1HjFLGHRDba',
            'constraint' => 0,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-06-05 08:44:27'),
        ]);

        // Isabel Thuss
        DB::table('users')->insert([
            'first_name' => 'Isabel',
            'last_name' => 'Thuss',
            'email' => 'isabelthuss@hotmail.com',
            'password' => '$2y$10$78Etd.MR.ujaS818fKnlleGETCmS0KkJNzh/Zi3fUroEORqiji9r6',
            'constraint' => 1,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-06-05 11:07:14'),
        ]);

        // Demi Hendriks
        DB::table('users')->insert([
            'first_name' => 'Demi',
            'last_name' => 'Hendriks',
            'email' => 'demihendriks1997@gmail.com',
            'password' => '$2y$10$Z5kubC50W.0pVXBeVCpDre.4fVveHBLtJPjb/6PGt/DPiSuZBdriq',
            'constraint' => 0,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-06-05 12:22:47'),
        ]);

    }
}
