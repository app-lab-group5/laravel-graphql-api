<?php

use App\Lesson;
use App\VocabWord;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LessonLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            "family-friends" => [
                1 => [
                    "woman",
                    "wife",
                    "son",
                    "sister",
                    "partner",
                ],
                2 => [
                    "male friend",
                    "girlfriend",
                    "friend",
                    "female friend",
                    "father",
                ],
                3 => [
                    "child",
                    "aunt",
                    "brother",
                    "daughter",
                    "family",
                ],
                4 => [
                    "grandma",
                    "grandpa",
                    "husband",
                    "man",
                    "mother",
                ],
                5 => [
                    "parents",
                    "people",
                    "person",
                    "uncle",
                ]
            ],
            "food" => [
                1 => [
                    "sausage",
                    "meat",
                    "jam",
                    "cookie",
                    "cheese",
                ],
                2 => [
                    "bread",
                    "butter",
                    "chocolat",
                    "donut",
                    "egg",
                ],
                3 => [
                    "orange (fruit)",
                    "pie",
                    "sugar",
                ]
            ],
            "animals" => [
                1 => [
                    "bird",
                    "dog",
                    "hamster",
                    "cat",
                    "mouse",
                ],
                2 => [
                    "parrot",
                    "rabbit",
                    "rat",
                ]
            ],
            "colours" => [
                1 => [
                    "black",
                    "brown",
                    "green",
                    "grey",
                    "orange (colour)",
                ],
                2 => [
                    "pink",
                    "purple",
                    "red",
                    "white",
                ]
            ],
            "buildings" => [
                1 => [
                    "office",
                    "hotel",
                    "hospital",
                    "church",
                    "bank",
                ],
                2 => [
                    "movie theater",
                    "museum",
                    "restaurant",
                    "school",
                    "station",
                    "theater",
                    "university"
                ]
            ],
            "emotions" => [
                1 => [
                    "funny",
                    "happy",
                    "content",
                    "pain",
                    "proud"
                ],
                2 => [
                    "angry",
                    "bad",
                    "boring",
                    "calm",
                    "curious",
                ],
                3 => [
                    "shy",
                    "sad",
                    "nervous",
                    "in love",
                    "jealous",
                ]
            ],
            "holidays" => [
                1 => [
                    "vacation",
                    "christmas",
                    "easter",
                    "newyear"
                ]
            ],
            "jobs" => [
                1 => [
                    "job",
                    "police",
                    "teacher",
                ],
                2 => [
                    "director",
                    "baker",
                    "butcher",
                    "dentist",
                    "doctor",
                ]
            ],
            "other" => [
                1 => [
                    "free",
                    "early",
                    "cold",
                    "birthday",
                    "a lot",
                ],
                2 => [
                    "beautiful",
                    "better",
                    "mail",
                    "maybe",
                    "money",
                ],
                3 => [
                    "god",
                    "good/correct",
                    "good luck",
                    "hard of hearing",
                    "if",
                ],
                4 => [
                    "media",
                    "interesting",
                    "hot",
                    "from you/yours",
                    "from me/mine",
                ],
                5 => [
                    "already",
                    "also",
                    "always",
                    "another",
                    "answer",
                ],
                6 => [
                    "world",
                    "weather",
                    "vegetarian",
                    "too early",
                    "question(s)",
                ],
                7 => [
                    "please",
                    "politics",
                    "printer",
                    "sex",
                    "sign language"
                ],
                8 => [
                    "name",
                    "new",
                    "number",
                    "old",
                    "or",
                ],
                9 => [
                    "ill(ness)",
                    "important",
                    "joke",
                    "letter",
                    "life",
                ],
                10 => [
                    "break",
                    "construction",
                    "culture",
                    "dead",
                    "deaf",
                    "fight(n)",
                    "flower"
                ],
            ],
            "time" => [
                1 => [
                    "month",
                    "morning",
                    "november",
                    "now",
                    "october",
                ],
                2 => [
                    "saturday",
                    "september",
                    "spring",
                    "summer",
                    "sunday",
                ],
                3 => [
                    "july",
                    "june",
                    "march",
                    "may",
                    "monday",
                ],
                4 => [
                    "thursday",
                    "today",
                    "tuesday",
                    "wednesday",
                    "week",
                ],
                5 => [
                    "good evening",
                    "good morning",
                    "good night",
                    "hour",
                    "january",
                ],
                6 => [
                    "december",
                    "evening",
                    "february",
                    "friday",
                    "good afternoon",
                ],
                7 => [
                    "afternoon",
                    "april",
                    "august",
                    "date",
                    "day",
                ],
                8 => [
                    "weekend",
                    "winter",
                    "year",
                ],
            ],
            "vehicles" => [
                1 => [
                    "airplane",
                    "bicycle",
                    "bus",
                    "car",
                    "metro",
                    "train"
                ]
            ],
            "verbs" => [
                1 => [
                    "to drink",
                    "to drive",
                    "to eat",
                    "to email",
                    "to fall",
                ],
                2 => [
                    "to mean",
                    "to not care",
                    "to read",
                    "to sleep",
                    "to sport",
                ],
                3 => [
                    "to build",
                    "can",
                    "cannot",
                    "to cook",
                    "to dream",
                ],
                4 => [
                    "(to) hear(ing)",
                    "to help",
                    "to know",
                    "to learn",
                    "to marry",
                ],
                5 => [
                    "(to) loan",
                    "(to) mistake",
                    "(to) soccer",
                    "(to) work",
                    "to boil",
                ],
                6 => [
                    "to start",
                    "to search",
                    "to sit",
                    "to walk",
                    "to travel",
                    "to understand",
                    "to write",
                ]
            ],
            "drinks" => [
                1 => [
                    "beer",
                    "coffee",
                    "juice",
                    "milk",
                    "tea",
                    "water",
                    "wine",
                ]
            ],
            "pronounce" => [
                1 => [
                    "how much/many",
                    "how",
                    "what",
                    "when",
                    "where",
                ],
                2 => [
                    "which",
                    "who",
                    "why",
                ]
            ],
            "home" => [
                1 => [
                    "toilet",
                    "room",
                    "living",
                    "kitchen",
                    "address",
                ],
                2 => [
                    "bed",
                    "chair",
                    "door",
                    "home",
                    "roof",
                ],
                3 => [
                    "table",
                    "television",
                    "window",
                ]
            ],
        ];


        foreach ($categories as $categoryName => $lessonLevels) {
            $lesson = Lesson::where('name', $categoryName . " lesson")->first();

            foreach ($lessonLevels as $levelKey => $levelValues) {

                $level = $lesson->lessonLevels()->create([
                    'difficulty_level' =>  $levelKey,
                ]);

                foreach ($levelValues as $vocabWord) {
                    $word = VocabWord::where('word_string', $vocabWord)->first();
                    $word->lessonLevel()->associate($level);
                    $word->save();
                }
            }

        }




        /*
        $this->seedLessonLevels($familyFriendsLevels, "family-friends lesson");
        $this->seedLessonLevels($foodLevels, "food lesson");
        $this->seedLessonLevels($colourLevels, "colours lesson");
        $this->seedLessonLevels($animalLevels, "animals lesson");
        $this->seedLessonLevels($homeLevels, "home lesson"); */

    }

    private function seedLessonLevels($arr, $lessonName) {

        $lesson = Lesson::where('name', $lessonName)->first();
        foreach ($arr as $levelKey => $levelValue) {

            $level = $lesson->lessonLevels()->create([
                'difficulty_level' =>  $levelKey,
            ]);

            foreach ($levelValue as $vocabWord) {
                $word = VocabWord::where('word_string', $vocabWord)->first();
                $word->lessonLevel()->associate($level);
                $word->save();
            }
        }
    }



}
